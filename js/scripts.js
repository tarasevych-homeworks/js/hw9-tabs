document.addEventListener('DOMContentLoaded', function(e){
    'use strict';
    let list = document.querySelectorAll('.centered-content a');
    list = Array.prototype.slice.call(list, 0); // convert nodeList to Array
    list.forEach(function(el) {
        el.addEventListener('click', function(){
            e.preventDefault();
            let tab = document.querySelector(el.getAttribute('href'));

            document.querySelector('.centered-content .active')
                .classList.remove('active');
            document.querySelector('.tabs-content .active')
                .classList.remove('active');

            el.classList.add('active');
            tab.classList.add('active');
        })
    })
})
